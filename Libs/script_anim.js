function Cath() {
    var container, camera, scene, renderer;
    var clock = new THREE.Clock();
    function onWindowResize() {
    camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize( window.innerWidth, window.innerHeight );
    }
    function Initialisation() {
        container = document.createElement( 'div' );
        document.body.appendChild( container );

        camera = new THREE.PerspectiveCamera( 90, window.innerWidth / window.innerHeight, 1, 2000 );
        camera.rotation.z=0;
        camera.rotation.x=Math.PI/2;
        camera.rotation.y=0;
        camera.position.z=-20;

        // initialisation de la scene
        scene = new THREE.Scene();

        var ambient = new THREE.AmbientLight( 0xffffff );
        scene.add( ambient );

        // creation de la texture

        var manager = new THREE.LoadingManager();
        manager.onProgress = function ( item, loaded, total ) {
            console.log( item, loaded, total );
        };

        var texture = new THREE.Texture();

        var loader = new THREE.ImageLoader( manager );
        loader.load( '../TranseptSud/TranseptTexture4096.jpg', function ( image ) {
            texture.image = image;
            texture.needsUpdate = true;
        } );

        // Chargement du modèle
        loader = new THREE.OBJLoader( manager );
        loader.load( '../TranseptSud/transeptSudBox.obj', function ( object ) {
            object.traverse( function ( child ) {
                if ( child instanceof THREE.Mesh ) {
                    child.material.map = texture;
                }
            } );
            scene.add( object );
        } );
        renderer = new THREE.WebGLRenderer();
        renderer.setSize( window.innerWidth, window.innerHeight );
        container.appendChild( renderer.domElement );
        window.addEventListener( 'resize', onWindowResize, false );

        // Gestion des déplacements
        controls = new THREE.OwnPersonControls( camera, renderer.domElement );
    }
    function Afficher() {
        controls.update( clock.getDelta() );
        renderer.render(scene,camera);
    }
    function Animer() {
        requestAnimationFrame(Animer);
        Option1();
        Afficher();
    }

    function Option2()
    {
        scene.fog = new THREE.Fog(0x000000, 0.8, 4.2); // assombrissement de l'image (gestion de l'éclairage)
    }

    function Option1() //limite les deplacements à la structure (pas de possibilité de sortie)
    {
        console.log("x="+camera.position.x+",y="+camera.position.y);
        if(camera.position.x>=3.95)
        {
            camera.position.x=3.95;
        }
        if(camera.position.x<=-3.95)
        {
            camera.position.x=-3.95;
        }
        if(camera.position.y>=10.45)
        {
            camera.position.y=10.45;
        }
        if(camera.position.y<=-10.45)
        {
            camera.position.y=-10.45;
        }
    }
    Initialisation();
    Option2();
    Animer();
}